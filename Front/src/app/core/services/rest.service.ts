import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError, mergeAll, Observable, throwError, map} from "rxjs";
import {environment} from "../../../environments/environment.development";

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(
    private http: HttpClient,
  ) { }

  get(method: string, params: any = {}): Observable<any> {
    return this.request('GET', method, params, null);
  }

  post(method: string, params: any = {}): Observable<any> {
    return this.request('POST', method, params);
  }

  put(method: string, params: any = {}): Observable<any> {
    return this.request('PUT', method, params);
  }

  delete(method: string, params: any = {}): Observable<any> {
    return this.request('DELETE', method, params)
  }

  upload(method: string, fileKey: string, file: File): Observable<any> {
    const formData: FormData = new FormData();
    formData.append(fileKey, file, file.name);
    return this.request('POST', method, formData, null)
  }

  download(method: string): Observable<Blob> {
    if (!method.startsWith('/')) {
      method = '/' + method;
    }
    let httpMethod = 'GET'
    const url = environment.restUrl + method;
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/pdf');
    return this.http.request(httpMethod, url, {
      headers: headers,
      responseType: 'blob',
    })
  }

  private request(
    httpMethod: 'POST' | 'GET' | 'PUT' | 'DELETE',
    method: string,
    params: any,
    contentType: string | null = 'application/json; charset=UTF-8'
  ): Observable<any> {
    if (!method.startsWith('/')) {
      method = '/' + method;
    }


    const url = environment.restUrl + method;
    const options: { headers: HttpHeaders, withCredentials: boolean, params: any | undefined, body: any | undefined } = {
      body: undefined, params: undefined,
      headers: contentType != null ? new HttpHeaders({
        'Content-Type': contentType,
      }) : new HttpHeaders(),
      withCredentials: true
    };

    if (httpMethod === 'GET') {
      options['params'] = params;
    } else {
      options['body'] = params;
    }

    if (httpMethod === 'PUT' && params.id === undefined) {
    }

    return this.http.request(httpMethod, url, options).pipe(
      map((value: any) => {
        return value;
      })
    );
  }
}
