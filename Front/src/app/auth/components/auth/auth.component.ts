import { Component } from '@angular/core';
import {Router, RouterModule} from "@angular/router";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent {

  constructor(
    private router: Router,
  ) {
  }

  medicAuth(): void {
    this.router.navigate(['medic'])
  }

  apothecaryAuth(): void {
    this.router.navigate(['apothecary'])
  }
}
