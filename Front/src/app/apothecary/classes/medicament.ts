export class Medicament {
  medicamentId: number = 0
  medicamentName: string = ""
  releasedCount: number = 0
  count: number = 0
  remainsCount: number = 0
  debitedCount: number = 0

  static parse(obj: any): Medicament|undefined {
    if (obj === undefined || obj === null) return undefined

    const medicament = new Medicament()
    Object.assign(medicament, obj)
    medicament.remainsCount = medicament.count - medicament.releasedCount
    return medicament
  }
}
