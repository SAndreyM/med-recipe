import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApothecaryComponent } from './components/apothecary/apothecary.component';
import {FormsModule} from "@angular/forms";
import { ShowRecipeComponent } from './components/show-recipe/show-recipe.component';



@NgModule({
    declarations: [
        ApothecaryComponent,
        ShowRecipeComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
    ]
})
export class ApothecaryModule { }
