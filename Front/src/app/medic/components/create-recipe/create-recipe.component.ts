import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MedicamentsListComponent} from "../medicaments-list/medicaments-list.component";
import {MedicService} from "../../services/medic.service";
@Component({
  selector: 'app-create-recipe',
  templateUrl: './create-recipe.component.html',
  styleUrls: ['./create-recipe.component.css']
})
export class CreateRecipeComponent implements OnInit {
  reactiveForm: FormGroup = this.builder.group({});
  fileUrl?: string
  readonly: boolean = false

  @ViewChild("medicamentList") medicamentList?: MedicamentsListComponent
  constructor(
    private router: Router,
    private builder: FormBuilder,
    private service: MedicService,
  ) {
  }

  ngOnInit() {
    this.reactiveForm = this.builder.group({
      namePat: ['Иван', Validators.required],
      sNamePat: ['Иванов', Validators.required],
      tNamePat: ['Иванович', Validators.required],
      email: [null, Validators.email],
      agePat: [null, [Validators.required]],
      nameMed: ['Иван', Validators.required],
      sNameMed: ['Иванов', Validators.required],
      tNameMed: ['Иванович', Validators.required],
      validityPeriod: [null, Validators.required],
    });
  }
  
  test() {
    const ret = this.reactiveForm.getRawValue()
    this.medicamentList?.buildRecipe()
    ret["medicaments"] = this.medicamentList?.buildRecipe()
    this.service.createRecipe("/apothecary/createRecipe", ret).subscribe(response => {
      this.service.downloadRecipe("/apothecary/pdf/" + response).subscribe(response => {
        const file = new Blob([response], {type: 'application/pdf'});
        this.fileUrl = URL.createObjectURL(file);
        this.readonly = true;
        window.open(this.fileUrl, '_blank', 'width=1000, height=800');
      })
    }
    )
  }

  printRecipe() {
    window.open(this.fileUrl, '_blank', 'width=1000, height=800');
  }

  back() {
    this.router.navigate(['medic'])
  }

}
