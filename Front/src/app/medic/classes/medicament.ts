export class Medicament {
  id: number = 0;
  name: string = "";
  selected: boolean = false;

  static parse(obj: any): Medicament|undefined {
    if (obj === undefined || obj === null) return undefined

    const medicament = new Medicament()
    Object.assign(medicament, obj)

    return medicament
  }
}
