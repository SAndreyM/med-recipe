import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './components/main-page/main-page.component';
import { CreateRecipeComponent } from './components/create-recipe/create-recipe.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ApothecaryModule} from "../apothecary/apothecary.module";
import { MedicamentsListComponent } from './components/medicaments-list/medicaments-list.component';
import {NgxPrintModule} from "ngx-print";



@NgModule({
  declarations: [
    MainPageComponent,
    CreateRecipeComponent,
    MedicamentsListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ApothecaryModule,
    NgxPrintModule,
  ]
})
export class MedicModule { }
