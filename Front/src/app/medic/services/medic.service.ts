import { Injectable } from '@angular/core';
import {RestService} from "../../core/services/rest.service";
import {Medicament} from "../classes/medicament";
import {map, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MedicService {

  constructor(
    private rest: RestService,
  ) { }

  createRecipe(url: string, body: any): Observable<string> {
    return this.rest.post(
      url,
      body
    ).pipe(
      map((value: any) => value.file)
    )
  }

  getMedicaments(url: string): Observable<Medicament[]> {
    return this.rest.get(
      url,
    ).pipe(
      map((value: any) => {
        const items: Medicament[] = value.med
          .map((obj: any) => Medicament.parse(obj))
          .filter((obj?: Medicament) => obj !== undefined)
        return items
      })
    )
  }

  downloadRecipe(url: string): Observable<Blob> {
    return this.rest.download(url)
  }
}
