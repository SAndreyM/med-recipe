from pathlib import Path

import flask
from flask import Flask
from flask_cors import CORS
from src.app.model import db
from src.app.model.recipe import Recipe, RecipeMedications

app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:qwerty@localhost/med-recipe'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)
#
# with app.app_context():
#     db.drop_all()
#     db.create_all()



for module in Path().glob('src/app/rest/**/*.py'):

    module = __import__(str(module)[:-3].replace('/', '.'), fromlist=['bp'])
    if hasattr(module, 'bp'):
        print(module.bp)
        app.register_blueprint(module.bp)

@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  response.headers.add('Access-Control-Allow-Credentials', 'true')
  return response

@app.route('/')
def hello_world():  # put application's code here
    # student_john = Recipe(fNamePat="test", sNamePat="test", tNamePat="test", agePat=2123573523,
    #                    fNameMed="test", sNameMed="test", tNameMed="test", validityPeriod=2123573523)
    # db.session.add(student_john)
    # db.session.commit()
    #print(student_john.id)
    resp = flask.Response("Foo bar baz")
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp


if __name__ == '__main__':
    app.run()
