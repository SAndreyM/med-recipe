from sqlalchemy import Table, Column, MetaData, Integer
from sqlalchemy.orm import relationship

from src.app.model import db

metadata = MetaData()

class Medicaments(db.Model):
    medId = db.Column(db.Integer, primary_key=True)
    medName = db.Column(db.String, nullable=False)


RecipeMedications = Table('recipe_medications', db.metadata,
                          Column('recipeId', Integer(), nullable=False),
                          Column('medicamentId', Integer(), nullable=False),
                          Column('count', Integer(), nullable=False),
                          Column('releasedCount', Integer(), nullable=False)
                          )

class Recipe(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    fNamePat = db.Column(db.String, nullable=False)
    sNamePat = db.Column(db.String, nullable=False)
    tNamePat = db.Column(db.String, nullable=False)
    emailPat = db.Column(db.String)
    agePat = db.Column(db.Float)

    fNameMed = db.Column(db.String, nullable=False)
    sNameMed = db.Column(db.String, nullable=False)
    tNameMed = db.Column(db.String, nullable=False)
    validityPeriod = db.Column(db.Float)

    notValidity = db.Column(db.Boolean)
