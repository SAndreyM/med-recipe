from fpdf import FPDF, HTMLMixin, YPos, XPos
import qrcode

def generatePDF(id, fio, date):
    pdf = FPDF()
    pdf.add_page()
    pdf.add_font('DejaVu', '', 'src/app/pdfGenerator/font/DejaVuSansCondensed.ttf')
    pdf.set_font('DejaVu', '', 14)


    data = f"{id}"


    img = qrcode.make(data)
    img.save("qrCode.png")

    pdf.image("qrCode.png", 10, 8, 33)

    # Добавляем адрес
    pdf.cell(50)
    pdf.cell(0, 5, f'Рецепт №{id}', new_y=YPos.NEXT, new_x=XPos.LEFT)
    pdf.cell(0, 5, f'ФИО пациента: {fio}', new_y=YPos.NEXT, new_x=XPos.LEFT)
    pdf.cell(0, 5, f'Дата окончания действия рецепта: {date}', new_y=YPos.NEXT, new_x=XPos.LEFT)

    pdf.output(f"./recipes/{id}.pdf")
    return f"{id}.pdf"
