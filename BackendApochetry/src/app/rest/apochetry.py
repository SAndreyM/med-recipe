import datetime
import random
import string
import time
from typing import Dict

import sqlalchemy
from flask import Blueprint, request, g, send_file

from src.app.model.recipe import Medicaments, Recipe, RecipeMedications
from src.app.model import db
from src.app.pdfGenerator.pdfGenerator import generatePDF

bp = Blueprint('apothecary', __name__, url_prefix='/apothecary')


@bp.route('/checkRecipeExist/<int:recipe_id>')
def check_recipe(recipe_id):
    recipe = db.session.query(Recipe).filter(Recipe.id == recipe_id).first()
    if recipe is None:
        return {"status": "notExist"}
    return {"status": "exist"}


@bp.route('/checkOut', methods=['POST'])
def check_out():
    form = request.json
    recipeIdCol = sqlalchemy.sql.column('recipeId')
    medicamentIdCol = sqlalchemy.sql.column('medicamentId')

    for medicament in form["debitMedicaments"]:
        stmt = RecipeMedications.update()\
            .where(recipeIdCol == form["recipeId"], medicamentIdCol == medicament["id"])\
            .values(releasedCount=medicament["count"])
        with db.engine.connect() as conn:
            result = conn.execute(stmt)
            conn.commit()
    return {"status": True}


@bp.route('/getRecipe/<int:recipe_id>')
def get_recipe(recipe_id):
    recipe = db.session.query(Recipe).filter(Recipe.id == recipe_id).first()
    if recipe is None:
        return {"status": "notExist"}

    now = datetime.datetime.now().date()
    now = time.mktime(now.timetuple())

    if recipe.validityPeriod < now:
        recipe.notValidity = True
        db.session.commit()

    req = db.session.query(Medicaments)
    ret = {}
    for medicament in req:
        ret[medicament.medId] = medicament.medName

    recipeIdCol = sqlalchemy.sql.column('recipeId')

    stmt = RecipeMedications.select().where(recipeIdCol == recipe_id)
    result = None
    with db.engine.connect() as conn:
        result = conn.execute(stmt)
    medicaments = [{
        "medicamentId": x.medicamentId,
        "medicamentName": ret[x.medicamentId],
        "count": x.count,
        "releasedCount": x.releasedCount
    } for x in result]

    flag = False
    for check in medicaments:
        if check["count"] > check["releasedCount"]:
            flag = True
            break

    status = ""
    if not flag:
        status = "fullReleased"
    elif recipe.notValidity:
        status = "notValidity"
    else:
        status = "valid"

    return {
        "status": status,
        "recipeInfo": {
            "namePat": recipe.fNamePat,
            "sNamePat": recipe.sNamePat,
            "tNamePat": recipe.tNamePat,
            "agePat": datetime.datetime.utcfromtimestamp(recipe.agePat).strftime('%d.%m.%Y'),
            "validityPeriod": datetime.datetime.utcfromtimestamp(recipe.validityPeriod).strftime('%d.%m.%Y'),
            "medicaments": medicaments
        }
    }


@bp.route("/pdf/<string:filename>", methods=['GET'])
def return_pdf(filename):
    return send_file(f'./recipes/{filename}')


@bp.route("/createRecipe", methods=['POST'])
def create_recipe():
    form = request.json
    form["agePat"] = time.mktime(datetime.datetime.strptime(form["agePat"], "%Y-%m-%d").timetuple())
    form["validityPeriod"] = time.mktime(datetime.datetime.strptime(form["validityPeriod"], "%Y-%m-%d").timetuple())
    recipe = Recipe(fNamePat=form["namePat"], sNamePat=form["sNamePat"], tNamePat=form["tNamePat"],
                    agePat=form["agePat"],
                    fNameMed=form["nameMed"], sNameMed=form["sNameMed"], tNameMed=form["tNameMed"],
                    validityPeriod=form["validityPeriod"],
                    notValidity=False)

    db.session.add(recipe)
    db.session.commit()
    for medicament in form["medicaments"]:
        stmt = RecipeMedications.insert().values(recipeId=recipe.id, medicamentId=medicament["id"],
                                                 count=medicament["count"], releasedCount=0)
        with db.engine.connect() as conn:
            result = conn.execute(stmt)
            conn.commit()
    return {"file": generatePDF(recipe.id, f'{form["namePat"]} {form["sNamePat"]} {form["tNamePat"]}',
                                datetime.datetime.utcfromtimestamp(form["validityPeriod"]).strftime('%d.%m.%Y'))}


@bp.route('/getMedicaments', methods=['GET'])
def get_medicaments() -> Dict:
    req = db.session.query(Medicaments)
    ret = {"med": []}
    for medicament in req:
        ret["med"].append({"id": medicament.medId, "name": medicament.medName})
    return ret
